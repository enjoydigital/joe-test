# Enjoy Digital Technical Test

## Junior Developer

### Instructions

1.  Clone this repo and download the code
2.  Wire up the course listing page at [http://localhost:58694/courses ](http://localhost:58694/courses) After pressing the search button it should list all the courses (basic controller and views are already in place). It should also link through to the course detail page.
3.  Integrate the apply form with the CRM REST API described at [http://app-technicaltestapi-dev.azurewebsites.net/swagger/](http://app-technicaltestapi-dev.azurewebsites.net/swagger/). You should use the /api/students/search resource to see if a student exists with the email address entered on the form then create or update the student accordingly. A basic service StudentService (Services\StudentService.cs) class has been provided.. Please implement the methods and use it in the controller.
4.  Push back your changes to the GIT repository    

This should be achievable in around 2-3 hours. We aren't looking for perfection just the ability to work stuff out on your own and follow a brief. Don't worry if you can't complete all the tasks or if it takes longer.  
  
Any questions or problems please give me a ring on 07947971492